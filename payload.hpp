#ifndef _PAYLOAD_H_
#define _PAYLOAD_H_

#include <stdint.h>

union int32union;
union int16union;
typedef uint8_t byte;

class Payload {
	private:
		byte *data;
		byte payloadSize;
		int maxSize;
		bool hasLimitedSize;
	public:
		Payload();
		Payload(int);
		bool addInteger(int32_t);
		bool addShort(int16_t);
		bool addByte(byte);
		void clear();
		byte* payload();
		int size();
		bool hasPayload();
		operator byte*();
		operator bool();
};

#endif
