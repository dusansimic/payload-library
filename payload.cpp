#include <stdlib.h>
#include <stdint.h>
#include "payload.hpp"

union int32_union {
	int32_t intData;
	byte byteData[4];
} int32_union;
union int16_union {
	int16_t shortData;
	byte byteData[2];
} int16_union;

Payload::Payload() {
	data = NULL;
	payloadSize = 0;
	hasLimitedSize = false;
}

Payload::Payload(int size) {
	data = new byte[size];
	payloadSize = 0;
	maxSize = size;
	hasLimitedSize = true;
}

bool Payload::addInteger(int32_t data) {
	if (this->hasLimitedSize && this->payloadSize + 4 > maxSize)
		return false;
	// Increase payload size and realloc data array
	this->payloadSize+=4;
	// If size is not limited we need to expand current array
	if (!this->hasLimitedSize)
		this->data = (byte*) realloc(this->data, this->payloadSize * sizeof(byte));
	// Load new data into union for converting
	int32_union.intData = data;
	// Create a pointer to insert new data
	byte* dataWalk = this->data + this->payloadSize - 4;

	// Insert new data
	for (byte i = 0; i < 4; i++)
		dataWalk[i] = int32_union.byteData[i];

	return true;
}

bool Payload::addShort(int16_t data) {
	if (this->hasLimitedSize && this->payloadSize + 2 > maxSize)
		return false;
	// Increase payload size and realloc data array
	this->payloadSize+=2;
	// If size is not limited we need to expand current array
	if (!this->hasLimitedSize)
		this->data = (byte*) realloc(this->data, this->payloadSize * sizeof(byte));
	// Load new data into union for converting
	int16_union.shortData = data;
	// Create a pointer to insert new data
	byte* dataWalk = this->data + this->payloadSize - 2;

	// Insert new data
	for (byte i = 0; i < 2; i++)
		dataWalk[i] = int16_union.byteData[i];

	return true;
}

bool Payload::addByte(byte data) {
	if (this->hasLimitedSize && this->payloadSize == maxSize)
		return false;
	// Increase payload size and realloc data array
	this->payloadSize++;
	// If size is not limited we need to expand current array
	if (!this->hasLimitedSize)
		this->data = (byte*) realloc(this->data, this->payloadSize * sizeof(byte));
	// Insert new data
	this->data[this->payloadSize - 1] = data;

	return true;
}

void Payload::clear() {
	// Clear data array
	this->payloadSize = 0;
	if (!this->hasLimitedSize)
		delete this->data;
}

byte* Payload::payload() {
	return this->data;
}

int Payload::size() {
	return (int) this->payloadSize;
}

bool Payload::hasPayload() {
	return this->payloadSize > 0;
}

Payload::operator byte*() {
	return payload();
}

Payload::operator bool() {
	return hasPayload();
}
